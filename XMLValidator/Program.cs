﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace XMLValidator
{
    class Program
    {
        static void Main(string[] args)
        {
            ValidateXSD();
        }

        static void ValidateXSD()
        {
            //string path2 = @"D:\Ilya\Реализация CDA документов\Разработка #757 Руководство по реализации СЭМД (174) Протокол инструментального исследования (СDA) Редакция 4\1.2.643.5.1.13.13.14.6.9-main\XSD_CDA_ПРОТОКОЛ_ИНСТРУМЕНТАЛЬНОГО_ИССЛЕДОВАНИЯ_Р4";
            //string path2 = @"D:\Ilya\Реализация CDA документов\Разработка #754 СЭМД (171)\27 октября 2023\1.2.643.5.1.13.13.15.43-main\XSD_CDA_МЕД_ЗАКЛ_О_НАЛИЧ_МЕД_ПРОТИВОП_ПОКАЗ_ОГРАНИЧ_К_УПРАВЛ_ТРАНСП_СРЕДСТВОМ_Р3";
            //string path2 = @"D:\Ilya\Реализация CDA документов\СЭМД (194)\(29.11.23)\1.2.643.5.1.13.13.15.47-main\XSD_CDA_МЕД_ЗАКЛ_ПО_РЕЗУЛЬТАТАМ_ПРЕДВАРИТ_(ПЕРИОДИЧ)_МЕД_ОСМОТРА_(ОБСЛЕД)_Р2";
            string path2 = @"D:\Ilya\Реализация CDA документов\(Актуал) Разработка #993 СЭМД (155) Справка об отсутствии медицинских противопоказаний для работы с использованием сведений, составляющих государственную тайну (CDA) Редакция 1\1.2.643.5.1.13.13.15.50-main\XSD_CDA_СПРАВКА_ОБ_ОТСУТСТВИИ_МЕД_ПРОТИВОП_ДЛЯ_РАБОТЫ_С_ГОС_ТАЙНОЙ_Р1";
            //string path2 = @"D:\Ilya\Реализация CDA документов\(Актуал) Разработка #992 СЭМД (188) Заключение медицинского учреждения о наличии отсутствии заболевания, препятствующего поступлению на гос. гражд. службу РФ и муниц. служб\1.2.643.5.1.13.13.14.54.9-project_r1\XSD_CDA_ЗАКЛЮЧЕНИЕ_МЕД_УЧР_О_НАЛИЧ_ОТСУТ_ЗАБОЛ_ПРЕПЯТ_ПОСТУПЛ_НА_ГОС_ГРАЖД_СЛУЖБУ_Р1";
            //string path2 = @"D:\Ilya\Реализация CDA документов\(Актуал) Разработка #991 СЭМД (196) Медицинская справка (врачебное профессионально-консультативное заключение) (CDA) Редакция 3\1.2.643.5.1.13.13.15.45-main\XSD_CDA_МЕДИЦИНСКАЯ_СПРАВКА_ФОРМА_086У_Р3";
            XmlReaderSettings booksSettings = new XmlReaderSettings();


            string xmlPath = @"D:\Ilya\GitLab\XMLValidator\CDA.xml";

            foreach (var file in Directory.GetFiles(path2))
            {
                booksSettings.Schemas.Add(null, file);
            }

            path2 += @"\coreschemas";
            foreach (var file in Directory.GetFiles(path2))
            {
                booksSettings.Schemas.Add(null, file);
            }

            booksSettings.ValidationType = ValidationType.Schema;
            booksSettings.ValidationEventHandler += booksSettingsValidationEventHandler;

            XmlReader books = XmlReader.Create(xmlPath, booksSettings);

            while (books.Read()) { }
        }

        static void booksSettingsValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
            {
                Console.Write(e.Exception.LineNumber);
                Console.Write("| WARNING: ");
                Console.WriteLine(e.Message);
            }
            else if (e.Severity == XmlSeverityType.Error)
            {
                Console.Write(e.Exception.LineNumber);
                Console.Write("| ERROR: ");
                Console.WriteLine(e.Message);
            }
        }
    }
}
